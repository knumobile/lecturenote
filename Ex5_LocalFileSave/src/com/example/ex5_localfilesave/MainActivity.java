package com.example.ex5_localfilesave;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}
	public void onButtonClick(View v) {
        Toast msg = null;
        switch (v.getId()) {
        case R.id.button1:
            EditText txt = (EditText)findViewById(R.id.editText1);
            String input = txt.getText().toString();
            try {
                FileOutputStream fos = openFileOutput("test.txt",
                    Context.MODE_WORLD_READABLE);
                fos.write(input.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                msg = Toast.makeText(this,
                    "파일 생성 실패\n파일을 찾을 수 없음",
                    Toast.LENGTH_SHORT);
                e.printStackTrace();
                break;
            } catch (IOException e) {
                msg = Toast.makeText(this,
                    "파일 생성 실패\n내용을 쓸 수 없음",
                    Toast.LENGTH_SHORT);
                e.printStackTrace();
                break;
            }
        	
            msg = Toast.makeText(this,
                "파일 생성",
                Toast.LENGTH_SHORT);
            
            InputMethodManager imm =
                    (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(findViewById(R.id.editText1).getWindowToken(), 0);

            break;
        case R.id.button2:
            TextView view = (TextView)findViewById(R.id.textView1);
            try {
                FileInputStream fis = openFileInput("test.txt");
                byte[] buff = new byte[fis.available()];
                while (fis.read(buff) != -1) {;}
                fis.close();
                view.setText(new String(buff));
            } catch (FileNotFoundException e) {
                msg = Toast.makeText(this,
                    "파일 읽기 실패\n파일을 찾을 수 없음",
                    Toast.LENGTH_SHORT);
                e.printStackTrace();
                break;
            } catch (IOException e) {
                msg = Toast.makeText(this,
                    "파일 읽기 실패\n내용을 읽을 수 없음",
                    Toast.LENGTH_SHORT);
                e.printStackTrace();
                break;
            }

        	
            msg = Toast.makeText(this,
                "파일 읽기",
                Toast.LENGTH_SHORT);
            break;
        case R.id.button3:
            if (deleteFile("test.txt")) {
                msg = Toast.makeText(this,
                    "파일 삭제 성공",
                    Toast.LENGTH_SHORT);
            } else {
                msg = Toast.makeText(this,
                    "파일 삭제 실패",
                    Toast.LENGTH_SHORT);
            }
            break;
        case R.id.button4:
        	String output = "파일 목록";
            String[] list = fileList();
            output += ": 파일 개수 " + list.length + "개";
            for (String file : list) {
                output += "\n";
                output += file;
            }
            TextView view2 = (TextView)findViewById(R.id.textView2);
            view2.setText(output);
            msg = Toast.makeText(this,
                "파일 목록 확인 성공",
                Toast.LENGTH_SHORT);
            break;
        }
        msg.show();
    }
}
