package com.example.ex5_preferencesave2;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

public class Main extends PreferenceActivity  {

    @Override
    protected void onCreate(final Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        getFragmentManager().beginTransaction().replace(
        android.R.id.content, new MainFragment()).commit();       
    }
    public static class MainFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref);        
        }
    }
}
